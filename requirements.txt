-i https://pypi.org/simple
argh==0.26.2
limits==1.3
pathtools==0.1.2
pika==0.12.0
python-logstash-async==1.4.1
pyyaml==3.13
six==1.11.0
watchdog==0.8.3
