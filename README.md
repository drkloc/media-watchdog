# Media Watchdog

Screen a path for updates and publish changes to rabbitmq

# Docker agnostic setup

## Install

+ Deploy a RabbitMQ instance
+ Deploy a Logstash instance
+ Install requirements with pipenv

```bash
# Pipenv
pipenv install
```

+ OR install requirements in a virtualenv with pip

```bash
makevirtualenv media-watchdog
workon media-watchdog
pip install -r requirements.txt
```

## Run

```bash
workon media-watchdog
MEDIA_WATCHDOG_FOLDER=~/Desktop/screenshots python mediawatch/watch_media_folder.py
```

This assumes you have:

+ A rabbitmq instance running on `0.0.0.0` with user `admin` and pass `admin123`
+ Logstash instance running on `0.0.0.0` and listening on port `5000`

If you need to specify the path or host/user/passwords for rabbitmq/logstash, preset the following environment variables:

+ `RABBITMQ_DEFAULT_USER`: defaults to `admin`
+ `RABBITMQ_DEFAULT_PASS`: defaults to `admin123`
+ `RABBITMQ_QUEUE`: defaults to `mediawatchdog`
+ `RABBITMQ_HOST`: defaults to `0.0.0.0`
+ `LOGSTASH_HOST`: Defaults to `0.0.0.0`
+ `LOGSTASH_PORT`: defaults to `5000`

# Docker setup with ahoy and docker compose

## Install

+ [Docker](https://docs.docker.com/install/)
+ [Docker Compose](https://docs.docker.com/compose/install/)
+ [ahoy](https://github.com/ahoy-cli/ahoy#installation)

## Run

```
export MEDIA_WATCHDOG_FOLDER=~/Desktop/screenshots
ahoy docker up
ahoy docker log watchdog
```

# Listen for changes (consume messages)

```python
import os
from pika import BlockingConnection
from pika.credentials import PlainCredentials
from pika.connection import ConnectionParameters

credentials = PlainCredentials(
    os.environ.get('RABBITMQ_DEFAULT_USER', 'admin'),
    os.environ.get('RABBITMQ_DEFAULT_PASS', 'admin123')
)
connection_parameters = ConnectionParameters(
    host=os.environ.get('RABBITMQ_HOST', '0.0.0.0'),
    credentials=credentials
)

connection = BlockingConnection(connection_parameters)
channel = connection.channel()
queue = os.environ.get('RABBITMQ_QUEUE', 'mediawatchdog')
channel.queue_declare(
    queue=queue
)


def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)


channel.basic_consume(callback,
                      queue=queue,
                      no_ack=True)

channel.start_consuming()
```
